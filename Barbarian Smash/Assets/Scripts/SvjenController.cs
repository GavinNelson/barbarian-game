﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SvjenController : CharacterController
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if (Input.GetMouseButtonDown(0) && Time.time > regCooldownCounter)
        {
            regCooldownCounter = Time.time + regACooldown;
            RegularAttack();

        }
        if (Input.GetMouseButtonDown(1) && Time.time > specCooldownCounter)
        {
            specCooldownCounter = Time.time + specACooldown;
            SpecialAttack();
        }
        if (Input.GetKeyDown(KeyCode.Q) && Time.time > teamCooldownCounter)
        {
            teamCooldownCounter = Time.time + teamACooldown;
            TeamAttack();
        }
        if (Input.GetKeyDown(KeyCode.R) && Time.time > rageCooldownCounter)
        {
            rageCooldownCounter = Time.time + rageACooldown;
            RageAttack();
        }
    }

    #region Svjen Actions
    public override void RegularAttack()
    {
        base.RegularAttack();
        Debug.Log("This is Svjen's Regular Attack.");
    }
    public override void SpecialAttack()
    {
        base.SpecialAttack();
        Debug.Log("This is Svjen's Special Attack.");
    }
    public override void TeamAttack()
    {
        base.TeamAttack();
        Debug.Log("This is Svjen's Team Attack.");
    }
    public override void RageAttack()
    {
        base.RageAttack();
        Debug.Log("This is Svjen's Rage Attack.");
    }
    #endregion
}
