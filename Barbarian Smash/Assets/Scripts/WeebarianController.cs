﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeebarianController : CharacterController
{




    // Start is called before the first frame update
    void Start()
    {
        speed = 28;
        regDamage = 1;
        specDamage = 2;
        teamDamage = 3;
        rageDamage = 4;
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if (Input.GetMouseButtonDown(0) && Time.time > regCooldownCounter)
        {
            regCooldownCounter = Time.time + regACooldown;
            RegularAttack();

        }
        if (Input.GetMouseButtonDown(1) && Time.time > specCooldownCounter)
        {
            specCooldownCounter = Time.time + specACooldown;
            SpecialAttack();
        }
        if (Input.GetKeyDown(KeyCode.Q) && Time.time > teamCooldownCounter)
        {
            teamCooldownCounter = Time.time + teamACooldown;
            TeamAttack();
        }
        if (Input.GetKeyDown(KeyCode.R) && Time.time > rageCooldownCounter)
        {
            rageCooldownCounter = Time.time + rageACooldown;
            RageAttack();
        }
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    #region Weeb Actions
    public override void RegularAttack()
    {
        Debug.Log("This is Weebarian's Regular Attack.");

    }
    public override void SpecialAttack()
    {
        Debug.Log("This is Weebarian's Special Attack.");
    }
    public override void TeamAttack()
    {
        Debug.Log("This is Weebarian's Team Attack.");
    }
    public override void RageAttack()
    {
        Debug.Log("This is Weebarian's Rage Attack.");
    }
    #endregion
}
