﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tempEnemyController : MonoBehaviour
{

    //THIS IS A TEMP SCRIPT TO HANDLE BASIC ENEMY STUFF


    public float curHealth = 100;

    public SpriteRenderer healthBar;

    // Start is called before the first frame update
    void Start()
    {
        healthBar = GetComponent<SpriteRenderer>();

    }

    // Update is called once per frame
    void Update()
    {
        healthBar.gameObject.transform.localScale = new Vector3((curHealth/10),0.5f,0);
    }
}
