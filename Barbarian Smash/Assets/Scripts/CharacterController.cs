﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{

    #region Public variables
    [Tooltip("This is the speed multiplier on the character")]
    public float speed;
    [Header("Attack Damage Numbers")]
    [Tooltip("This is the regular attack damage number")]
    public float regDamage;
    [Tooltip("This is the special attack damage number")]
    public float specDamage;
    [Tooltip("This is the team attack damage number")]
    public float teamDamage;
    [Tooltip("This is the rage attack damage number")]
    public float rageDamage;
    public Rigidbody2D player;
    #endregion

    #region Private Variables
    private Vector2 move;
    public float moveH;
    public float moveV;
    #endregion


    #region CooldownCounters
    //This is the variable used to check if the cooldown is over.
    [HideInInspector]
    public float regCooldownCounter = 0, specCooldownCounter = 0, teamCooldownCounter = 0, rageCooldownCounter = 10;
    #endregion


    #region Cooldown Timers
    //This handles the actual cooldown time.
    [Header("CooldownTimers")]
    [Tooltip("This is the regular attacks cooldown time")]
    public float regACooldown = 2f;
    [Tooltip("This is the special attacks cooldown time")]
    public float specACooldown = 4f;
    [Tooltip("This is the team attacks cooldown time")]
    public float teamACooldown = 4f;
    [Tooltip("This is the rage attacks cooldown time")]
    public float rageACooldown = 10f;
    #endregion


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public virtual void Update()
    {
        
    }

    public virtual void FixedUpdate()
    {
        Movement();
    }

    #region Actions
    public virtual void RegularAttack()
    {
        Debug.Log("The Regular Attack has been used");
    }

    public virtual void SpecialAttack()
    {
        Debug.Log("The Special Attack has been used");
    
    }

    public virtual void TeamAttack()
    {
        Debug.Log("The Team Attack has been used");

    }

    public virtual void RageAttack()
    {
        Debug.Log("The Rage Attack has been used");

    }

    public virtual void PickUpItem()
    {

    }
    #endregion


    public virtual void Movement()
    {
        //Vector3 move = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0.0f);

        //transform.position += move * speed * Time.deltaTime;

        moveH = Input.GetAxis("Horizontal") * speed;
        moveV = Input.GetAxis("Vertical") * speed;

        move = new Vector2(moveH, moveV);

        player.velocity = move * speed * Time.deltaTime;
    }

}
